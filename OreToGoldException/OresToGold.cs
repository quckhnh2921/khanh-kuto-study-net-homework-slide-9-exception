﻿class OresToGold
{
    const int TEN_ORES = 10;
    const int FIVE_ORES = 5;
    const int THREE_ORES = 3;
    const int PRICE_OF_FIRST_TEN_ORES = 10;
    const int PRICE_OF_NEXT_FIVE_ORES = 5;
    const int PRICE_OF_NEXT_THREE_ORES = 2;
    const int PRICE_OF_REMAINDER_ORES = 1;
    public int OreToGold(int numberOfOres)
    {
        try
        {
            if (numberOfOres < 0)
            {
                throw new Exception("Number of ores is smaller than 0");
            }
            else
            {
                int firstTenOres = Math.Min(numberOfOres, TEN_ORES);
                int priceOfFirstTenOres = firstTenOres * PRICE_OF_FIRST_TEN_ORES;
                numberOfOres -= firstTenOres;

                int nextFiveOres = Math.Min(numberOfOres, FIVE_ORES);
                int priceOfNextFiveOres = nextFiveOres * PRICE_OF_NEXT_FIVE_ORES;
                numberOfOres -= nextFiveOres;

                int nextThreeOres = Math.Min(numberOfOres, THREE_ORES);
                int priceOfNextThreeOres = nextThreeOres * PRICE_OF_NEXT_THREE_ORES;
                numberOfOres -= nextThreeOres;

                int remainder = numberOfOres;
                int priceOfRemainder = remainder * PRICE_OF_REMAINDER_ORES;

                int total = priceOfFirstTenOres + priceOfNextFiveOres + priceOfNextThreeOres + priceOfRemainder;
                return total;
            }
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return 0;
        }
    }
    public string CheckResult(int total, int numberOfOres)
    {
        if (total == 0)
        {
            return "Number " + numberOfOres + " is invalid";
        }
        else
        {
            return "You get " + total + " gold !";
        }
    }
}
