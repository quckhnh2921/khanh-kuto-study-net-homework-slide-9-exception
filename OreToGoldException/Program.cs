﻿try
{
    OresToGold oresToGold = new OresToGold();
    Console.Write("Enter number of ores: ");
    int numberOfOres = int.Parse(Console.ReadLine());
    var gold = oresToGold.OreToGold(numberOfOres);
    var checkGold = oresToGold.CheckResult(gold, numberOfOres);
    Console.WriteLine(checkGold);
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}
