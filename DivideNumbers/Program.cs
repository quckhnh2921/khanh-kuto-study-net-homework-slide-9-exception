﻿try
{
    DivideNumbers divide = new DivideNumbers();
    Console.Write("Enter divident: ");
    double divident = double.Parse(Console.ReadLine());
    Console.Write("Enter divisor: ");
    double divisor = double.Parse(Console.ReadLine());
    var result = divide.DivideNumber(divident, divisor);
    Console.WriteLine(divide.Result(result));
}
catch (Exception ex)
{
    Console.WriteLine(ex.Message);
}