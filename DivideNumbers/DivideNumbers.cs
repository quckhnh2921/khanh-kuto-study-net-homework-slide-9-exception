﻿class DivideNumbers
{
    public double DivideNumber(double divident, double divisor)
    {
        try
        {
            if(divisor == 0)
            {
                throw new Exception("Cannot divide by zero");
            }
            else
            {
                double result= divident / divisor;
                return Math.Round(result,2);
            }
        }catch(Exception ex)
        {
            Console.WriteLine(ex.Message) ;
            return 0;
        }
    }

    public string Result(double result)
    {
        if(result == 0)
        {
            return "Error !";
        }
        else
        {
            return "Result: "+result;
        }
    }
}