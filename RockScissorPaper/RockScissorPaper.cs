﻿class RockScissorPaper
{
    const int KEO = 1;
    const int BUA = 2;
    const int BAO = 3;
    const int DRAW = 0;
    const int PLAYER_1_WIN = 1;
    const int PLAYER_2_WIN = 2;
    const int ERROR = -1;

    public int GameRule(int player1choose, int player2choose)
    {
        try
        {
            if (player1choose < 1 || player1choose > 3 || player2choose < 1 || player2choose > 3)
            {
                throw new ArgumentException("Player choose is out of range from 1 to 3");
            }

            if (player1choose == KEO && player2choose == BAO
                || player1choose == BUA && player2choose == KEO
                || player1choose == BAO && player2choose == BUA)
            {
                return PLAYER_1_WIN;
            }

            if (player2choose == KEO && player1choose == BAO
                || player2choose == BUA && player1choose == KEO
                || player2choose == BAO && player1choose == BUA)
            {
                return PLAYER_2_WIN;
            }

            return DRAW;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            return ERROR;
        }
    }

    public string GameResult(int result)
    {
        if(result == -1)
        {
            return "Game Error !";
        }
        if(result == PLAYER_1_WIN)
        {
            return "Player 1 Win !";
        }
        if(result == PLAYER_2_WIN)
        {
            return "Player 2 Win !";
        }
        return "Draw !";
    }

    public void GameMenu()
    {
        Console.WriteLine("------ROCK-SCISSOR-PAPER------");
        Console.WriteLine("|         1: KEO             |");
        Console.WriteLine("|         2: BUA             |");
        Console.WriteLine("|         3: BAO             |");
        Console.WriteLine("------------------------------");
    }
}