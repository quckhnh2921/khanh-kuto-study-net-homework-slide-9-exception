﻿try
{
    RockScissorPaper game = new RockScissorPaper();
    game.GameMenu();
    Console.Write("Player 1 choose: ");
    int player1Choose = int.Parse(Console.ReadLine());
    Console.Write("Player 2 choose: ");
    int player2Choose = int.Parse(Console.ReadLine());
    var result = game.GameRule(player1Choose, player2Choose);
    Console.WriteLine(game.GameResult(result));
}
catch(Exception ex)
{
    Console.WriteLine(ex.Message);
}